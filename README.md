# Artifactory Integration with Bitbucket Pipeline
## Store build information and build artifacts to JFrog Artifactory

`To make this integration work you will need to have running Artifactory-pro/Artifactory SAAS.`

### Steps to build docker images using Circle CI and push it to Artifactory.

##### Step 1:

copy `bitbucke-pipeline.yml` to your project

##### Step 2:

Enable your project in Bitbucket Pipeline.

##### Step 3:

add Environment Variables `ARTIFACTORY_USERNAME`, `ARTIFACTORY_DOCKER_REPOSITORY` and `ARTIFACTORY_PASSWORD` in Environment Variables settings of Bitbucket Pipeline.
In this example `$ARTIFACTORY_DOCKER_REPOSITORY=jfrogtraining-docker-dev.jfrog.io`
![screenshot](img/Screen_Shot2.png)

##### Step 4:

You should be able to see published Docker image in Artifactory.
![screenshot](img/Screen_Shot3.png)

Also Build information.

![screenshot](img/Screen_Shot4.png)
## Note: `This solution only supports Artifactory with valid ssl as Bitbucket Pipeline does not support insecure docker registry `
